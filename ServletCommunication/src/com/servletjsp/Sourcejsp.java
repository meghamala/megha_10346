package com.servletjsp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Sourcejsp
 */
@WebServlet("/Sourcejsp")
public class Sourcejsp extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Sourcejsp() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
     protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// getting the methods from html page
		String pwd1 = request.getParameter("password");
		String pwd2 = request.getParameter("pwd2");
		//creating a string of list object
		List<String> books = new ArrayList<String> ();
		// adding the elements to the list
		books.add("java");
		books.add(".net");
		books.add("c/c++");
		books.add("QE");
 		// requesting to the another page
		RequestDispatcher rd = request.getRequestDispatcher("firstjsp.jsp");
		if(pwd1.equals(pwd2)) {
			request.setAttribute("mybooks", books);
			rd.forward(request, response);
			 
		} else {
			response.sendRedirect("error.html");
		}

	
	}
}
