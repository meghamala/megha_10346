package com.Servletcommunication;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class DiaplayServlet is used to get the request from
 * the source servlet and display the response to web page
 */
@WebServlet("/DiaplayServlet")
public class DiaplayServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DiaplayServlet() {
		super();

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// to get the parameters from the web page
		String firstname = request.getParameter("firstname");
		String email = request.getParameter("email");
		// displaying the response object
		PrintWriter ps = response.getWriter();
		ps.println("<html><body>");
		ps.println("<h4>" + firstname + "</h4>");
		ps.println("<h5>" + email + "</h5>");
		ps.println("</body></html>");

		ps.close();

	}

}
