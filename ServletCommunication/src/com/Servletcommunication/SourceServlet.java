package com.Servletcommunication;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class SourceServlet is used to display the sample
 * servlet to communicate between the servlets
 */
@WebServlet("/SourceServlet")
public class SourceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SourceServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// getting the parameters from the web page
		String password = request.getParameter("password");
		String pwd2 = request.getParameter("pwd2");
		// displaying the response object
		PrintWriter ps = response.getWriter();
		// using requestDispatcher to communicate with the target
		// servlet(diaplayservlet)
		RequestDispatcher rd = request.getRequestDispatcher("DiaplayServlet");
		ps.println("<html><body>");

		if (password.equals(pwd2)) {
			rd.forward(request, response);

		} else {
			rd.include(request, response);

		}
		ps.println("</body></html>");

		ps.close();

	}

}
