<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>firstjsp</title>
</head>
<body>
	<%
		String username = request.getParameter("username");
		String email = request.getParameter("email");
		List<String> mybooks = (List<String>) request.getAttribute("mybooks");
	%>

	<h3>

		Hello :
		<%=username%>
	</h3>
	<h4>
		Email :
		<%=email%></h4>
	<table>
		<tr>
			<th>Books</th>
		</tr>
		<%
			for (String book : mybooks) {
		%>
		<tr>
			<td><%=book%></td>
		</tr>
		<%
			}
		%>

	</table>
</body>
</html>