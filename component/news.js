import React, { Component } from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as newsActions from '../store/actions/newsActions';
import counter from './counter';
class News extends Component{

constructor(props){
    super(props);
}
componentDidMount(){
    this.props.newsActions.fetchNews();
}
render(){
    return(
        <React.Fragment>
            {
                (this.props.newsItems)
            ?<div>
                {this.props.newsItems.map(item=>(
                    <div key ={item._id}>
                    <h2>{item.heading}</h2>
                    </div>
                ))}
            </div>
            : <div>
        
                loading.....
            </div>
            }
        </React.Fragment>
    );
}

 }
 function mapStateToProps(state){
     return{
         newsItems:state.news,
     };
 }
 function mapDispatchToProps(dispatch){
    return{
        newsActions:bindActionCreators(newsActions,dispatch),
    };
}
export  default connect(mapStateToProps,mapDispatchToProps)(News);