import * as allActions from '../actions/actionConstant';
const initialState ={
    is_logged_in :false,

}
 export default function authReducer(state = initialState,action){
     switch(action.type){
         case allActions.LOGIN_USER_SUCCESS:
      
         return {
             ...state,
             is_logged_in: true,
         }
         default :
          return state ;   
     }
 }