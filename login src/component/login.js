import React, { Component } from 'react';
import './login.css';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as authActions from '../store/actions/authActions';
class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            isSubmitted: false

        };
        this.handleChange = this.handleChange.bind(this);
        this.doLogin = this.doLogin.bind(this);
    }



    handleChange = e => {
        this.setState({ [e.target.name]: e.target.value });
        e.preventDefault();
    }


    // componentDidMount() {
    //     this.props.newsActions.fetchNewsByID( this.props.match.params.id);
    // }
    // componentWillReceiveProps(newProps){
    //     if(newProps.newsItem){
    //         this.setState({
    //             newsItem: newProps.newsItem,

    //         })
    //     }
    // }

    doLogin = e => {
          
        console.log("login entered");
        this.props.authActions.doLoginUser({
            email: this.state.email,
            password: this.state.password
        })

        this.setState({
            isSubmitted: true
        })
    }
    render() {
        if (this.state.isSubmitted) {
            console.log(localStorage.getItem("jwt-token") );
            if (localStorage.getItem("jwt-token") && (this.props.isLoggedIn === true)) {
            
                console.log("token established and the user is validated");
                return (
                    <Redirect to ="/logindetails"/>
                )
            }
            else {
                return (
                    <div>Not a valid credentials</div>
                )
            }
        }
        // else{
        //     return(
        //         <div>Your are not logged in </div>
        //     )
        // }
        return (


            <div>
                <div style={{backgroundColor:"white"}}>
                <div class="row">
                {/* <div class="col-lg-12" style={{backgroundColor:"white",height:"100px"}} >
                      <div class ="col-lg-4">
                
                      </div>                    
                         </div>
                            <br></br> */}
                      
                      <div class ="col-lg-12">
                    <div class="container" style={{ backgroundColor: "lightblue", width: "500px", height: "300px" }}>
                      
                        <br></br>
                        <div style={{ border: "1pxsolidwhite", height: "200px", width: "500px", marginLeft: "100px" }} >
                      
                            <div class ="formgroup">
                            
                                <label class="col-sm-2"> Email :</label>
                                   <div class ="col-sm-7">
                                    <input type="text" name="email" value={this.state.email} placeholder="enter the username" onChange={this.handleChange} />
                                    </div>
                                <br></br>
                                <br></br>
                                <label class="col-sm-2"> Password:</label>
                                <div class ="col-sm-7">
                                       <input type="password" name="password" value={this.state.password} placeholder="enter the password" onChange={this.handleChange} />
                                       </div>
                                <br></br>
                            <div class ="col-sm-10">
                                
                                <a href="#href" id="tag1" target="_blank"> <i>ForgotPassword?</i> </a><br></br>
                                </div>
                                </div>
                                <br></br>
                            <div class ="col-lg-12">
                            <input type="submit" value="Submit" onClick={this.doLogin} />
                            </div>
                            </div>
                        </div>
                    </div>
                    </div>
             
                    </div>  

            </div>
        );
    }

}
function mapStateToProps(state) {
    console.log(state)
    return {
        isLoggedIn: state.authReducer.is_logged_in,
    };
}
function mapDispatchToProps(dispatch) {
    return {
        authActions: bindActionCreators(authActions, dispatch),
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(Login);