import React, { Component } from 'react';

class DisplayAllProducts extends Component {
    constructor(props) {
        super(props);
        this.state = {
            products: [],
            isLoaded: false
        }
    }
    componentDidMount() {
        fetch('http://192.168.150.65:8080/RetailBillingWebServer/rest/manager/displayproduct')
            .then(response => response.json())
            .then(productsData => {
                this.setState({
                    isLoaded: true,
                    items: productsData
                })
            });
    }
    render() {
        var { isLoaded, products } = this.state;
        if (!isLoaded) {
            return <div>Loading...</div>;
        }
        return (
            <div>
                
                <ul>
                    {products.map(product => (
                        <li key="{item.id}">
                            Product Id: {product.product_id} | 
                            Product Name: {product.productname} |
                            Price : {product.price} |
                            Quantity : {product.quantity} |
                            productType : {product.productType}
                        </li>
                    ))}
                </ul>
            </div>
        );
    }
}
export default DisplayAllProducts;