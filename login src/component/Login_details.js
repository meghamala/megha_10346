import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as roleActions from '../store/actions/roleActions';
import {Redirect} from 'react-router-dom';
class LoginDetails extends Component {

    constructor(props) {
        super(props);
        this.state={
          valid:false,
    }
    }
    componentDidMount() {
        this.props.roleActions.fetchNewsByRole();
    }


    logout = e => {
        localStorage.removeItem('jwt-token');
        this.setState ({
            valid : true
        })

    }

    render() {
   
        if(this.state.valid==true){
            return(
            <Redirect to ='/'/>
            )
        }
        return (

            <div>
                {/* ROLE :{this.props.UserDetails}
                {
                    (this.props.userDetails)
                        ? <div>
                            {this.props.userDetails.role}
                            <div>
                            
                            </div>
                        </div>
                        
                        : <div>
                            Loading...
                        </div>
                } */}
                <button onClick={this.logout}>Logout</button>
            </div>
        )
    }
}
function mapStateToProps(state) {
    return {
        userDetails: state.roleReducer.UserDetails,
    };
}
function mapDispatchToProps(dispatch) {
    return {
        roleActions: bindActionCreators(roleActions, dispatch),
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(LoginDetails);