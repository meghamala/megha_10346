import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as newsActions from '../store/actions/newsActions';
import counter from './counter';
import './news.css';
class NewsDetails extends Component {

    constructor(props) {
        super(props);
        this.state={
            newsItem:{}
        }
    }
    componentDidMount() {
        this.props.newsActions.fetchNewsByID( this.props.match.params.id);
    }
    componentWillReceiveProps(newProps){
        if(newProps.newsItem){
            this.setState({
                newsItem: newProps.newsItem,

            })
        }
    }
    render() {
        return (
            <React.Fragment>
             
              <div class="row"> 
              <div class="container">
              <div class="col-lg-12" style={{textAlign:"justify"}}>
              {this.state.newsItem.body}
              </div>
              <div>
          <p style={{textAlign:"right"}}>  <b>publishDate :</b>{ this.state.newsItem.timestamp}</p>
              </div>
              </div>
            
              </div>
          
            </React.Fragment>
        );
    }

}
    function mapStateToProps(state) {
        return {
            newsItem: state.newsItem,
        };
    }
    function mapDispatchToProps(dispatch) {
        return {
            newsActions: bindActionCreators(newsActions, dispatch),
        };
    }
    export default connect(mapStateToProps, mapDispatchToProps)(NewsDetails);