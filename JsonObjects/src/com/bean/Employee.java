package com.bean;
/**
 * 
 * @author Megha
 *class to getter setter for updation & delete
 */
public class Employee {
     private int id;
		private  String name;
		private String address;
		private int age;

		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public int getAge() {
			return age;
		}
		public void setAge(int age) {
			this.age = age;
		}
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
}
