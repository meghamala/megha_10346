package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bean.Employee;
import com.service.UpdationService;
import com.service.UpdationServiceimplementation;
import com.util.JsonConverter;

/**
 * Servlet implementation class DisplayAllstudents
 */
@WebServlet("/DisplayAllstudents")
public class DisplayAllstudents extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DisplayAllstudents() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
   		
   		response.setContentType("application/json;charset=UTF-8");

   		PrintWriter pw =response.getWriter(); 
   		UpdationService service  = new UpdationServiceimplementation ();
   		List<Employee> empList = service.fetchAllStudents();
   		JsonConverter jsonConverter = new JsonConverter();
   		String result = jsonConverter.convertToJson(empList);
   		pw.println(result);
   		pw.close();
   	}

}
