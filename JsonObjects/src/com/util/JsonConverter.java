package com.util;

import java.util.List;


import com.bean.Employee;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.mysql.cj.xdevapi.JsonArray;

public class JsonConverter {

private final Gson gson;
    
    public JsonConverter() {
        
        gson = new GsonBuilder().create();
    }

    public String convertToJson(List<Employee> emplist) {
        
        com.google.gson.JsonArray jarray = gson.toJsonTree(emplist).getAsJsonArray();
        JsonObject jsonObject = new JsonObject();
        jsonObject.add("emplist", jarray);

        return jsonObject.toString();
    }
	
	
}













