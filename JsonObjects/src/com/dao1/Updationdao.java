package com.dao1;

import java.util.List;

import com.bean.Employee;

/**
 * 
 * @author MEGHA this interface to display the details
 */
public interface Updationdao {

	boolean deleteById(int id);

	boolean updateEmployee(Employee employee);

	List<Employee> getAllEmployees();
}
