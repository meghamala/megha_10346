package com.dao1;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.bean.Employee;
import com.util.DBUtil;

/**
 * this class is used to display update the student & delete the record
 */
public class UpdationClass implements Updationdao {

	/**
	 * method to fetch the details
	 */
	public List<Employee> getAllEmployees() {

		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		Employee emp = null;
		List<Employee> emplist = new ArrayList<Employee>();
		String sql = "select * from employee";
		try {
			con = DBUtil.getCon();
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				emp = new Employee();
				emp.setId(rs.getInt(1));
				emp.setName(rs.getString(2));
				emp.setAddress(rs.getString(3));
				emp.setAge(rs.getInt(4));
				emplist.add(emp);
			}
		} catch (SQLException e) {

		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return emplist;
	}

	/**
	 * method to update the details
	 */

	public boolean updateEmployee(Employee employee) {
		Connection con = null;
		PreparedStatement ps = null;

		int id = employee.getId();
		String name = employee.getName();
		int age = employee.getAge();
		boolean result = false;
		String sql = "update employee SET emp_name = ?, emp_age = ? where emp_id = ?";
		try {
			con = DBUtil.getCon();
			System.out.println("");
			ps = con.prepareStatement(sql);
			ps.setString(1, name);
			ps.setInt(2, age);
			ps.setInt(3, id);
			int rowsEffected = ps.executeUpdate();
			if (rowsEffected == 1) {
				result = true;
			}

		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return result;
	}

	/**
	 * method to delete the student
	 */
	public boolean deleteById(int id) {
		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		String sql = "DELETE FROM employee WHERE emp_id = ?";
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			int rowsEffected = ps.executeUpdate();
			if (rowsEffected == 1) {
				result = true;
			}
		} catch (SQLException e) {

		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}
	



}
