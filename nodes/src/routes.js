import React, { Component } from 'react';
import { Switch, Route, BrowserRouter } from 'react-router-dom';


import Register from './component/register';
import Home from './component/home';
import Clock from './component/clock';
import CarExample from './component/carExample';
import CarDetails from './component/carDetails';
const Routes = () => (
    <BrowserRouter>
        <Switch>
            <Route path={'/'} component={Home} exact />
            <Route path={'/clock'} component={Clock} />
            <Route path ={'/carExample'} component={CarExample} exact/>
            <Route path ={'/carExample/:id'} component={CarDetails}exact/>
            <Route path={'/register'} component={Register} />
        </Switch>

    </BrowserRouter>
);
export default Routes;