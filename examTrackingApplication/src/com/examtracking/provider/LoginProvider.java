package com.examtracking.provider;

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.examtracking.bean.SetLoginDetails;
import com.examtracking.bean.StudentDetails;

import com.examtracking.dao.LoginDetailsData;
import com.examtracking.dao.StudentData;
import com.examtracking.service.UpdateStudent;
import com.examtracking.service.UserLoginImpl;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@Path("/adminfunctions")
public class LoginProvider {
	/**
	 * This is resource method which performs login operation
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	@POST
	@Path("/login")
	@Produces("text/plain")
	public String login(@QueryParam("username") String username, @QueryParam("password") String password) {

		boolean result = false;
		UserLoginImpl login = new UserLoginImpl();
		result = login.loginUser(username, password);
		if (result) {
			return "login successful";
		} else {
			return "login unsuccessful";
		}
	}

	@GET
	@Path("/searchbyid")
	@Produces("application/json")
	@Consumes("application/json")
	public String getStudentById(@QueryParam("stud_id") long stud_id) {
		Gson gson = new Gson();

		StudentDetails result = new StudentData().getStudentByid(stud_id);

		String studentJsonString = gson.toJson(result);

		return studentJsonString;

	}

	/**
	 * This is resource method which performs register the student operation
	 * 
	 * @param student json object
	 * @return
	 */
	@POST
	@Path("/registerStudent")
	@Produces("application/json")
	@Consumes("text/plain")
	public String registerStudent(String student) {
		String str = null;
		Gson gson = new Gson();
		StudentDetails studentData = gson.fromJson(student, StudentDetails.class);		
		boolean result = new UpdateStudent().registerNewStudent(studentData);
		if (result) {
			str = "student_record_inserted";
		}
		return str;
	}

	/**
	 * This is resource method which performs login operation
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	@POST
	@Path("/studentlogin")
	@Produces("text/plain")
	public String setLoginDetails(String student) {

		String result = null;
		Gson gson = new Gson();

		JsonParser parser = new JsonParser();
		JsonElement jsonTree = parser.parse(student);
		JsonObject jsonObject = jsonTree.getAsJsonObject();

		JsonElement f1 = jsonObject.get("stud_id");
		long stud_id = f1.getAsLong();

		List<SetLoginDetails> list = new ArrayList<SetLoginDetails>();

		SetLoginDetails studentData = gson.fromJson(student, SetLoginDetails.class);
		studentData.setStud_id(stud_id);
		list.add(studentData);

		result = UserLoginImpl.SignUpStudent1(stud_id);

		if (result.equalsIgnoreCase("true")) {
			System.out.println(result);
			result = LoginDetailsData.saveStudentRecord(list);
		}

		if (result != null) {
			result = "student_login_details_Set";
		}
		return result;
	}
	//TODO ADD FORGOT PASSWORD METHOD FOR BOTH ADMIN AND STUDENT

	//TODO ADD RESET PASSWORD METHOD FOR STUDENT
}
