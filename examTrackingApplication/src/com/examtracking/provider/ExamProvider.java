package com.examtracking.provider;

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import com.examtracking.bean.SetExamTimeTable;
import com.examtracking.service.ExamDetailsImpl;
import com.google.gson.Gson;

public class ExamProvider {

	/**
	 * This is resource method which is used to add new Time table in database
	 * 
	 * @param time table
	 * @return
	 */
	@POST
	@Path("/setexamtimetable")
	@Produces("application/json")
	@Consumes("text/plain")
	public String setTimeTable(String timetableList) {
		String str = null;
		Gson gson = new Gson();
		SetExamTimeTable timeTableData = gson.fromJson(timetableList, SetExamTimeTable.class);
		
		//For testing purpose hardcoded values
		timeTableData.setBranchId(1);
		timeTableData.setExamTypeId(2);
		
		List<SetExamTimeTable> list = new ArrayList<SetExamTimeTable>();
		list.add(timeTableData);
		boolean result = new ExamDetailsImpl().SetExamTimeTable(list);
		if (result) {
			str = "time_table_created";
		}
		return str;
	}

	/**
	 * This is resource method which is return time table from database
	 * 
	 * @param time table
	 * @return
	 */
	@GET
	@Path("/getexamtimetable")
	@Produces("application/json")
	@Consumes("text/plain")
	public String getTimeTable(@QueryParam("branchId") String branchId, @QueryParam("examChoice") String examChoice) {
		int branch_id = Integer.parseInt(branchId);
		int exam_choice = Integer.parseInt(examChoice);
		Gson gson = new Gson();
		List<SetExamTimeTable> list = new ExamDetailsImpl().getAllExamTimeTable(branch_id, exam_choice);
		String studentJsonString = gson.toJson(list);
		return studentJsonString;
	}
	
}
