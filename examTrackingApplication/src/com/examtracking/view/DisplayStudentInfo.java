package com.examtracking.view;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import com.examtracking.bean.StudentDetails;
import com.examtracking.util.DBConnection;

/**
 * @author BATCH -'C' this class is used to display the student
 *         information(Login details&details)
 *
 */
public class DisplayStudentInfo {
	static List<StudentDetails> list = new ArrayList<StudentDetails>();

	/**
	 * This method print complete Student records from the database
	 * 
	 */
	public static void printStudentAllRecords() {
		try {
			Connection con = DBConnection.getDBConnection();

			Statement stmt = con.createStatement();

			ResultSet rs = stmt.executeQuery("select * from student_details");

			System.out.println(
					"=======================================================================================================================================================================================================================================");
			System.out.printf("%5s %28s %28s %28s %20s %20s %30s %15s %20s %20s", "Student ID", "First Name",
					"Last Name", "Email-Id", "Mobile", "Parent Mobile", "Address", "Branch", "DOB", "DOJ");
			System.out.println();
			System.out.println(
					"=======================================================================================================================================================================================================================================");
			while (rs.next()) {
				System.out.format("%5s %30s %30s %30s %20s %20s %30s %15s %20s %20s", rs.getInt(1), rs.getString(2),
						rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7),
						rs.getString(8), rs.getString(9), rs.getString(10));
				System.out.println();
			}
			System.out.println(
					"---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method is used to display the studentLOginDetails
	 * 
	 */
	public static void printLoginAllRecords() {
		try {
			Connection con = DBConnection.getDBConnection();

			Statement stmt = con.createStatement();

			ResultSet rs = stmt.executeQuery("select * from student_credentials");

			System.out.println(
					"==============================================================================================================================");
			System.out.printf("%5s %28s %28s %28s ", "Student ID1", "user name", "password", "security ques");
			System.out.println();
			System.out.println(
					"==============================================================================================================================");
			while (rs.next()) {
				System.out.format("%5s %30s %30s %30s", rs.getInt(1), rs.getString(2), rs.getString(3),
						rs.getString(4));
				System.out.println();
			}
			System.out.println(
					"------------------------------------------------------------------------------------------------------------------------------");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
