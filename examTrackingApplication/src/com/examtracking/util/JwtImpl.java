package com.examtracking.util;
import java.security.Key;
import io.jsonwebtoken.*;
import io.jsonwebtoken.impl.crypto.MacProvider;

import java.util.Base64;
import java.util.Date;
import java.util.UUID;
public class JwtImpl {
	
	private static final Key secret = MacProvider.generateKey(SignatureAlgorithm.HS256);
    private static final byte[] secretBytes = secret.getEncoded();
    private static final String base64SecretBytes = Base64.getEncoder().encodeToString(secretBytes);
    
    /**
     * This Method Generates new Token
     * @param username
     * @return token
     */
    public static String generateToken(String username) {
    	
        String id = UUID.randomUUID().toString().replace("-", "");
        Date now = new Date();
        Date exp = new Date(System.currentTimeMillis() + (1000 * 30)); // 30 seconds

        String token = Jwts.builder()
            .setId(id)
            .setIssuedAt(now)
            //adding user data into token payload
            .setSubject(username)
            .setNotBefore(now)
            .setExpiration(exp)
            .signWith(SignatureAlgorithm.HS256, base64SecretBytes)
            .compact();

        return token;
    }
    
    /**
     * This Method Verify token provided by client
     * @param token
     * @return token Id
     */
    public static String verifyToken(String token) {
        Claims claims = Jwts.parser()
            .setSigningKey(base64SecretBytes)
            .parseClaimsJws(token).getBody();
//        System.out.println("----------------------------");
//        System.out.println("ID: " + claims.getId());
//        System.out.println("Subject: " + claims.getSubject());
//        System.out.println("Issuer: " + claims.getIssuer());
//        System.out.println("Expiration: " + claims.getExpiration());
		return claims.getId();
    }

//    public static void main(String[] args) {
//        System.out.println(generateToken());
//        System.out.println(generateToken());
//        String token = generateToken();
//        String token2 = generateToken();
//        verifyToken(token);
//        verifyToken(token2);
//    }
	
//	//Sample method to construct a JWT
//	private String createJWT(String id, String issuer, String subject, long ttlMillis) {
//	 
//	    //The JWT signature algorithm we will be using to sign the token
//	    SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
//	 
//	    long nowMillis = System.currentTimeMillis();
//	    Date now = new Date(nowMillis);
//	 
//	    //We will sign our JWT with our ApiKey secret
//	    byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(apiKey.getSecret());
//	    Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());
//	 
//	    //Let's set the JWT Claims
//	    JwtBuilder builder = Jwts.builder().setId(id)
//	                                .setIssuedAt(now)
//	                                .setSubject(subject)
//	                                .setIssuer(issuer)
//	                                .signWith(signatureAlgorithm, signingKey);
//	 
//	    //if it has been specified, let's add the expiration
//	    if (ttlMillis >= 0) {
//	    long expMillis = nowMillis + ttlMillis;
//	        Date exp = new Date(expMillis);
//	        builder.setExpiration(exp);
//	    }
//	 
//	    //Builds the JWT and serializes it to a compact, URL-safe string
//	    return builder.compact();
//	}
}
