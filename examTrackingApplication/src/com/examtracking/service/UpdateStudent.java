package com.examtracking.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.examtracking.bean.StudentDetails;
import com.examtracking.controller.StudentMenu;

import com.examtracking.dao.StudentData;
/**
 * 
 * @author BATCH-'C'
 * this class is used to update the student details
 *
 */

public class UpdateStudent {
     // creating the list for the student details
	static List<StudentDetails> list = new ArrayList<StudentDetails>();
	/**
	 * This method used to update the student details
	 *@return true if the student details are updated 
	 */
	public static boolean checkStudentForUpdate(long id) {

	
		boolean flag = false;
		try {
			list = StudentData.getStudentList();
			Iterator<StudentDetails> it = list.iterator();
			
			while (it.hasNext()) {
				StudentDetails st1 = (StudentDetails) it.next();
				if (st1.getStudent_id() == id) {
					flag = true;
					StudentMenu.getStudentForUpdate(id);
					break;
				}
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		return flag;
	}
	
	public boolean registerNewStudent(StudentDetails studentData) {
		List<StudentDetails> list = new ArrayList<StudentDetails>();
		list.add(studentData);
		boolean result = StudentData.saveStudentRecord(list);
		return result;
	}
}
