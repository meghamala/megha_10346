package com.examtracking.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import com.examtracking.bean.AcademicInfo;
import com.examtracking.bean.SetExamTimeTable;
import com.examtracking.bean.SetMarkSheet;
import com.examtracking.bean.StudentDetails;
import com.examtracking.controller.ExamPortal;
import com.examtracking.dao.ExamData;
import com.examtracking.dao.StudentData;

/**
 * 
 * @author BATCH-'C' this class used to get and set the examination details like
 *         eligibility check, timetable,mark sheet of the student
 *
 */
public class ExamDetailsImpl {

	/**
	 * This method for checking attendance for eligibility of exam or not
	 * 
	 * @param attendance
	 * @return percentage
	 */
	public static double setAttendance(long attendance) {

		double percentage = (attendance * 100.0) / 150;

		return percentage;

	}

	/**
	 * This method for checking fee details of student for eligibility of exam or
	 * not
	 * 
	 * @param fee
	 * @return true if student is eligible otherwise return false
	 */
	public static boolean setFee(String fee) { // method to check fee status of a student eligibility of exam or no
		if (fee.equalsIgnoreCase("paid")) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * this method used to check both attendance and fee details of the student to
	 * check the eligibility check
	 * 
	 * @param setattendance
	 * @param setFee
	 * @return true if student is eligible otherwise return false
	 */
	public static boolean check(boolean setattendance, boolean setFee) {
		if (setattendance && setFee) {
			return true;
		}
		return false;
	}

	/**
	 * this method display the eligibility criteria of the student
	 * 
	 * @param academicInfo
	 */
	public static  boolean setStudentEligibility(AcademicInfo academicInfo) {

		boolean result=ExamData.setAcademicRecord(academicInfo);
		return result;
	}

	/**
	 * this method is used to set the examtimetable
	 * 
	 * @param list
	 */
	public boolean SetExamTimeTable(List<com.examtracking.bean.SetExamTimeTable> list) {
		boolean result = false;
		if(checkTimeTable(list)) {
			result = new ExamData().saveExamTimeTable(list);
		}
		return result;
	}
	
	public List getAllExamTimeTable(int branch_id,int exam_choice) {
		List<SetExamTimeTable> list = new ExamData().getExamTimeTable(branch_id, exam_choice);
		return list;
	}
	/**
	 * This get Gets student basic information
	 * 
	 * @param id
	 * @return First name , last name , branch
	 */
	public static String[] getStudentNameBranch(long id) {
		@SuppressWarnings({ "rawtypes", "unchecked" })
		List<StudentDetails> list = new ArrayList();
		list = StudentData.getStudentList();
		String[] stdDetails = new String[3];
		if (list.size() != 0) {
			Iterator<StudentDetails> it = list.iterator();
			while (it.hasNext()) {
				StudentDetails st1 = (StudentDetails) it.next();
				if (st1.getStudent_id() == id) {
					stdDetails[0] = st1.getFirstname();
					stdDetails[1] = st1.getLastname();
					stdDetails[2] = st1.getBranch();
				}
			}
		} 
		return stdDetails;

	}
	/**
	 * This Method used to set the timetable based on the branch choice and exam choice
	 * If timetable is already created providing the option to reset the timetable
	 * 
	 * @param branchChoice
	 * @param examChoice
	 */
	@SuppressWarnings("resource")
	public boolean checkTimeTable(List<com.examtracking.bean.SetExamTimeTable> list) {
		@SuppressWarnings("unused")
		int branchChoice = 0;
		int examChoice = 0;
		boolean result = false;
//		Iterator<com.examtracking.bean.SetExamTimeTable> itr = list.iterator();
		for(SetExamTimeTable time : list) {
			branchChoice = time.getBranchId();
			examChoice = time.getExamTypeId();
		}
		//creating the list for exam time table to retrieve the data
		List<SetExamTimeTable> timeList = ExamData.getAllTimeTable();	
		
		//checking if the list size is empty or not to set the time table		
		if (timeList.size() <= 0) {
			result = true;
		} 
		//if the list is not empty providing the option to reset the timetable
		
		else {
			Iterator<SetExamTimeTable> itr = timeList.iterator();
			boolean flag = false;
			while (itr.hasNext()) {
				SetExamTimeTable tb = itr.next();
				if (tb.getExamTypeId() == examChoice && tb.getBranchId() == branchChoice) {
					result = false;
					break;
				} else {
					result = true;
				}
			}	
		}
		return result;
	}
	/**
	 * This method checks whether student attended exam or not
	 * 
	 * @param examChoice
	 */
	public boolean checkMarks(int student_id,int examChoice) {
		boolean result = false;
		if (new ExamPortal().checkEligible(student_id)) {
			result = true;
		}
		return result;
	}
	/**
	 * This method used to set the marks for the student
	 * 
	 * @param examchoice
	 * @param id
	 */
	public boolean setMarkSheetForStudent(SetMarkSheet marks , int student_id,int examChoice) {	
		List<SetMarkSheet> list = new ArrayList<SetMarkSheet>();
		list.add(marks);
		boolean result = ExamData.saveAllStudentMarksList(list, student_id, examChoice);	
		return result;
	}
	
}
