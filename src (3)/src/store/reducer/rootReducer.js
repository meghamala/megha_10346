import { combineReducers } from 'redux';

import counterReducer from './counterReducer';
 import newsReducer from './newsReducer';
 import authReducer from './authReducer';
 import roleReducer from './roleReducer';

 
const rootReducer = combineReducers({
  counterReducer,
  newsReducer,
  authReducer,
  roleReducer
  
});

export default rootReducer;
