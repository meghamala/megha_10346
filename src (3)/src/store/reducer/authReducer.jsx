import * as allActions from '../actions/actionConstant';
const initialState ={
    is_logged_in :false,
    statusCode:0

}
 export default function authReducer(state = initialState,action){
     switch(action.type){
         case allActions.DO_LOGIN_USER:
         return action;
         case allActions.LOGIN_USER_SUCCESS:
         return {
             ...state,
             is_logged_in: true,
         }

         case allActions.LOGIN_USER_DATA_ERROR:
         return {
             is_logged_in:false,
             statusCode :action.payload
         }

         default :
          return state ;   
     }
 }