import { createStore, applyMiddleware,compose } from 'redux';
import rootReducer from '../reducer/rootReducer';
import newsMiddleware from '../middlewares/newsService';
import authService from '../middlewares/authService';
import roleService from '../middlewares/roleService';


export default function configureStore(){
    return createStore(
        rootReducer,
        compose(applyMiddleware(
            newsMiddleware,authService,roleService
        ))
    );
};