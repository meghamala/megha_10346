import React, { Component } from 'react';
import './login.css';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as authActions from '../store/actions/authActions';
import Header from './header';
class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            isSubmitted: false

        };
        this.handleChange = this.handleChange.bind(this);
        this.doLogin = this.doLogin.bind(this);
    }



    handleChange = e => {
        this.setState({ [e.target.name]: e.target.value });
        e.preventDefault();
    }


    // componentDidMount() {
    //     this.props.newsActions.fetchNewsByID( this.props.match.params.id);
    // }
    // componentWillReceiveProps(newProps){
    //     if(newProps.newsItem){
    //         this.setState({
    //             newsItem: newProps.newsItem,

    //         })
    //     }
    // }

    doLogin = e => {

        console.log("login entered");
        this.props.authActions.doLoginUser({
            email: this.state.email,
            password: this.state.password
        })

        this.setState({
            isSubmitted: true
        })
    }
    render() {
        if (this.state.isSubmitted) {
            console.log(localStorage.getItem("jwt-token"));
            if (localStorage.getItem("jwt-token") && (this.props.isLoggedIn === true)) {

                console.log("token established and the user is validated");
                return (
                    <Redirect to="/RedirectPage" />
                )
            }
            else {
                if (this.props.statuscode == -1) {
                    alert("this is not valid active mail id");
                    this.setState({ isSubmitted: false });
                }
                else if (this.props.statuscode == -2) {
                    alert("this is not valid pasword format");
                    this.setState({ isSubmitted: false });
                }
                else if (this.props.statuscode == -3) {
                    alert("Invalid Password and email are incorrect");
                }
            }
        }

        return (
            <div>
                <div class="row">
                    <div class="header">
                        <div class="col-lg-12">
                           <Header/>
                        </div>

                    </div>

                </div>

               <br></br>
               <br></br>
                <div style={{ backgroundImage: "white", align: "center" }}>
                    <div class="row">
                        <div class="container" >
                            <div style={{ border: "1pxsolidwhite", height: "200px", width: "500px", marginLeft: "100px" }} >

                                <div class="formgroup">
                                    <form>
                                        <label class="col-sm-2"> Email :</label>
                                        <input type="text" name="email" value={this.state.email} placeholder="enter the username" onChange={this.handleChange} />

                                        <br></br>
                                        <br></br>
                                        <label class="col-sm-2"> Password:</label>
                                        <input type="password" name="password" value={this.state.password} placeholder="enter the password" onChange={this.handleChange} />

                                        <br></br>
                                        <a href="#href" id="tag1" target="_blank"> <i>ForgotPassword?</i> </a><br></br>
                                    </form>
                                </div>

                                <br></br>
                                <input type="submit" value="Submit" onClick={this.doLogin} />

                            </div>
                        </div>
                    </div>
                </div>



            </div>
        );
    }

}
function mapStateToProps(state) {
    console.log(state)
    return {
        isLoggedIn: state.authReducer.is_logged_in,
        statuscode: state.authReducer.statusCode
    };
}
function mapDispatchToProps(dispatch) {
    return {
        authActions: bindActionCreators(authActions, dispatch),
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(Login);