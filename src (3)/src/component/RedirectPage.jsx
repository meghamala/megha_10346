
import React, { Component } from 'react';

import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from '../store/actions/roleActions'
import * as authActions from '../store/actions/authActions';
class RedirectPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            

        };
    }
    componentDidMount(){
this.props.userActions.fetchNewsByRole()
    }

     render (){
         switch(this.props.user){
             case 'user':
             return <Redirect to ='/logindetails'>
             </Redirect>
             case 'admin':
             return <Redirect to ='/'>
             </Redirect>
             case 'employee':
             return <Redirect to ='/'>
             </Redirect>
             default :
             return null
         }
     }

    }
    function mapStateToProps(state) {
        console.log(state)
        return {
           user : state.roleReducer.role
        };
    }
    function mapDispatchToProps(dispatch) {
        return {
            userActions: bindActionCreators(userActions, dispatch),
        };
    }
    export default connect(mapStateToProps, mapDispatchToProps)(RedirectPage);

