import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './header.css'
class Header extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return ( 
            <div class="row">
                <div class="container-fluid">
                    <ul>
                        {/* <Link to='/counter'/><li>Counter</li><br></br>
                            <Link to='/news'/> <li>News</li><br></br>
                            <Link to='/news/:id'/><li>NewsLoading</li><br></br>
                            <Link to='/login'/><li>Login</li><br></br> */}
                        <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <Link class="nav-link" to="/counter">Counter</Link>
                                </li>
                                <li class="nav-item">
                                    <Link class="nav-link" to="/news">News</Link>
                                </li>
                                <li class="nav-item">
                                    <Link class="nav-link" to="/news/:id">NewsLoading</Link>
                                </li>
                                
                                <li class="nav-item">
                                
                                    <Link class="nav-link" to="/login">Login</Link>
                                </li>
                            </ul>
                            
                        </nav>
                    </ul>
                </div>

            </div>
        );
    }
}
export default Header;