package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.bean.Employee;
import com.util.DBUtil;

/**
 * 
 * @author MEGHA this class is used to implement the insert method into database
 */
public class EmployeeDAOImp implements EmployeeDAO {
	/**
	 * this method is used to
	 */

	public boolean createEmployee(Employee employee) {

		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		// query to the database
		String sql = "insert into employee values(?,?,?,?)";
		try {
			// setting the elements
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, employee.getId());
			ps.setString(2, employee.getName());
			ps.setString(3, employee.getAddress());
			ps.setInt(4, employee.getAge());
			// to rows effected
			int rowsEffected = ps.executeUpdate();
			if (rowsEffected == 1) {
				result = true;
			}

		} catch (SQLException e) {

		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return result;
	}

	/**
	 * this method to get the employee id
	 */
	public Employee searchById(int id) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Employee emp = null;
		String sql = "select *from employee  where emp_id = ?";
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			while (rs.next()) {
				emp = new Employee();
				emp.setId(rs.getInt(1));
				emp.setName(rs.getString(2));
				emp.setAddress(rs.getString(3));
				emp.setAge(rs.getInt(4));

			}
		} catch (SQLException e) {

		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return emp;
	}

	/**
	 * this method to get the employee list
	 */
	public List<Employee> getAllEmployees() {

		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		Employee emp = null;
		List<Employee> emplist = new ArrayList<Employee>();
		String sql = "select * from employee";
		try {
			con = DBUtil.getCon();
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				emp = new Employee();
				emp.setId(rs.getInt(1));
				emp.setName(rs.getString(2));
				emp.setAddress(rs.getString(3));
				emp.setAge(rs.getInt(4));
				emplist.add(emp);
			}
		} catch (SQLException e) {

		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return emplist;
	}

}
