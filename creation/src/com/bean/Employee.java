package com.bean;
/**
 * 
 * @author MEGHA
 * this class is used to get the elements of employee ta ble from the database
 */
public class Employee {
//getter setter methods
	
		private int id;
		private  String name;
		private String address;
		private int age;

	/**
	 * 
	 * @return id
	 */
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		/**
		 * 
		 * @return name
		 */
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		/**
		 * 
		 * @return age
		 */
		public int getAge() {
			return age;
		}
		public void setAge(int age) {
			this.age = age;
		}
		/**
		 * 
		 * @return address 
		 */
		public String getAddress() {
			return address;
		}
			
		public void setAddress(String address) {
			this.address = address;
		}
}
