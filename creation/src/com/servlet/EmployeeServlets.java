package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bean.Employee;
import com.service.EmployeeService;
import com.service.EmployeeServiceImp1;

/**
 * Servlet implementation class EmployeeServlets
 */
@WebServlet("/EmployeeServlets")
public class EmployeeServlets extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EmployeeServlets() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// requesting the  methods to get the values
		int emp_id= Integer.parseInt(request.getParameter("id"));
		String emp_name = request.getParameter("name");
		String emp_addr = request.getParameter("addr");
		int Employee_Age = Integer.parseInt(request.getParameter("age"));

		PrintWriter out = response.getWriter();
		
	    Employee employee =new Employee();
	    employee.setId(emp_id);
		employee.setName(emp_name);
		employee.setName(emp_addr);
        employee.setAge(Employee_Age);
       
        out.print("<html><body>");
        out.print("<table>");
		out.print("<tr><th><td> "  +employee.getId()+"</td></th>");
		out.print("<th><td> "  +employee.getName()+"</td></th>");
		out.print("<th><td> "  +employee.getAddress()+"</td></th>");
		out.print("<th><td> "  +employee.getAge()+"</td></th></tr>");
	    out.print("</table>");
		
	    // implementing service method by calling the class
	    EmployeeService empservice= new EmployeeServiceImp1();
		boolean result = empservice.insertEmployee(employee);
	        if(result ){  
	            out.print("<p>Record saved successfully!</p>");  
	     
	        }else{  
	            out.println("Sorry! unable to save record");  
	        }  
	
		out.print("</body></html>");
		out.close();
		
		
	}
}
