package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bean.Employee;
import com.service.EmployeeService;
import com.service.EmployeeServiceImp1;

/**
 * Servlet implementation class DisplayServlet
 */
@WebServlet("/DisplayServlet")
public class DisplayServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DisplayServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    
   	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	   	
   		EmployeeService service = new  EmployeeServiceImp1();
   		List<Employee> studentList = service.fetchAllStudents();
   		
   		Iterator<Employee> i= studentList.iterator();
   		
   		PrintWriter out = response.getWriter();
   		out.print("<html><body>");
   		out.println("<table border='1'>");
   		out.println("<tr><th>ID</th><th>Name</th> <th>Address</th><th>Age</th></tr>");
   		while(i.hasNext()) {
   			Employee employee =i.next();
   			out.println("<tr><td>"+employee.getId()+"</td>");
   			out.println("<td>"+employee.getName()+"</td>");
   			out.println("<td>"+employee.getAddress()+"</td>");
   			out.println("<td>"+employee.getAge()+"</td></tr>");
   		}
   		out.println("</table>");
   		out.print("</body></html>");
		out.close();
   	
   		
   		
   		
   	}

	

}
