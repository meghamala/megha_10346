package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bean.Employee;
import com.service.EmployeeService;
import com.service.EmployeeServiceImp1;

/**
 * Servlet implementation class SearchServlet
 */
@WebServlet("/SearchServlet")
public class SearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchServlet() {
        super();
     
    }


	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int employee_id = Integer.parseInt(request.getParameter("id"));
		 EmployeeService empservice= new EmployeeServiceImp1();
		     Employee emp = empservice.findById(employee_id);
	
		     PrintWriter res = response.getWriter();
		     res.print("<html><body>");
		     if(emp != null)
		     {
		        res.print("<h3> id exists</h3>");
		    	res.print("<p> "  +emp.getId()+"</p>");
		 		res.print("<p> "  +emp.getName()+"</p>");
		 		res.print("<p> "  +emp.getAddress()+"</p>");
		 		res .print("<p> "  +emp.getAge()+"</p>");
		    	 
		     }
		     else {
		    	 res.println("<h3> id not found</h3>");
		     }
		     res.print("</body></html>");
	}

}
