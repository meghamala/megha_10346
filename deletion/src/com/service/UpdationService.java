package com.service;

import java.util.List;

import com.bean.Employee;

/**
 * this service interface to implement the methods
 * 
 * @author Megha
 *
 */
public interface UpdationService {

	boolean deleteById(int id);

	boolean updateEmployee(Employee employee);

	List<Employee> fetchAllStudents();
}
