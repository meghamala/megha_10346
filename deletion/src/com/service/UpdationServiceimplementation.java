package com.service;

import java.util.List;

import com.bean.Employee;

import com.dao1.UpdationClass;
import com.dao1.Updationdao;

/**
 * 
 * @author megha 
 * to implement the service methods to update &delete the employee
 *         details
 */
public class UpdationServiceimplementation implements UpdationService {

	Updationdao updated = new UpdationClass();

	/**
	 * this method implements to fetch the data
	 */
	public List<Employee> fetchAllStudents() {

		Updationdao updated = new UpdationClass();
		@SuppressWarnings("unused")
		List<Employee> emplist = updated.getAllEmployees();

		return null;
	}

	/**
	 * this method implements to update the details of the student
	 */
	public boolean updateEmployee(Employee employee) {
		Updationdao update = new UpdationClass();
		boolean result = update.updateEmployee(employee);

		return result;
	}

	/***
	 * this method implements to delete the student
	 *
	 */
	public boolean deleteById(int id) {
		Updationdao update = new UpdationClass();
		boolean result = update.deleteById(id);

		return result;
	}

}
