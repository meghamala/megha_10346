package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.service.UpdationService;
import com.service.UpdationServiceimplementation;

/**
 * Servlet implementation class Deleteservlet
 */
@WebServlet("/Deleteservlet")
public class Deleteservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Deleteservlet() {
        super();
   
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int employee_id= Integer.parseInt(request.getParameter("id"));
		
		UpdationService service = new UpdationServiceimplementation(); 
		//creating method to delete the employee
		boolean result = service.deleteById(employee_id);
	
		
		PrintWriter out = response.getWriter();
		
		out.print("<html><body>");
		
		if(result) {
			out.print("<h2>Student deleted</h2>");
		}
		else {
			out.print("<h2>Something wrong</h2>");
		}
		
		out.print("</body></html>");
		out.close();
		
		
	}

}
