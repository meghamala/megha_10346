package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bean.Employee;
import com.service.UpdationService;
import com.service.UpdationServiceimplementation;

/**
 * Servlet implementation class UpdationServlet
 */
@WebServlet("/UpdationServlet")
public class UpdationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdationServlet() {
        super();
    
    }

	/**
	 * @param id 
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 response.setContentType("text/html");

			int employee_id = Integer.parseInt(request.getParameter("id"));
			String employee_name = request.getParameter("name");
			int employee_age = Integer.parseInt(request.getParameter("age"));

			Employee employee = new Employee();
			employee.setId(employee_id);
		    employee.setName(employee_name);
			employee.setAge(employee_age);
       	   UpdationService service= new UpdationServiceimplementation();
		   boolean result = service.updateEmployee(employee);
		   PrintWriter out = response.getWriter();
		   out.println("<html><body>");
		   if(result)
		   {
			   out.println("<h2> Id Present Display updated details</h2>");
			   out.print(employee.getId());
			   out.print(employee.getName());
			   out.print(employee.getAddress());
			   out.print(employee.getAge());
		   }
		   else 
		   {
			   out.println("<h2> no results are found</h2>");
		   }
		
		   out.println("</body></html");
		
		
	}

}
