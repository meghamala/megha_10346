import { createStore, applyMiddleware,compose } from 'redux';
import rootReducer from '../reducer/newsReducer';
import newsMiddleware from '../middlewares/newsService';
import authService from '../middlewares/authService';
export default function configureStore(){
    return createStore(
        rootReducer,
        compose(applyMiddleware(
            newsMiddleware,authService
        ))
    );
};