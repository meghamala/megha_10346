import * as allActions from '../actions/actionConstant';
/**
 * const json varisbles are declared
 */
 const initialState  = {
     news :[],
     newsItem :{}
     
 }
 
 export default function newsReducer(state = initialState,action){
     switch(action.type){

         case allActions.FETCH_NEWS:
          console.log("datafetched");
         return action;


         case allActions.RECEIVE_NEWS:
           console.log("datareceived");
         return{

             ...state,
             news : action.payload.articles,

         };
         case allActions.FETCH_NEWS_BY_ID:
         console.log("fetchedbyId");
         return action;

         case allActions.RECEIEVE_NEWS_BY_ID:
         console.log("receives news byId");
         return{
             ...state,
             newsItem:action.payload.article
         }
         
         default : return state ;   
     }
 }