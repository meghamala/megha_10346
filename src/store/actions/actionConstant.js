export const INCREMENT= 'INCREMENT';
export const DECREMENT= 'DECREMENT';
/**
 *Action Constants for the Fetching News and Recieving News
 */

export const FETCH_NEWS ='FETCH_NEWS';
export const RECEIVE_NEWS  ='RECEIEVE_NEWS';
/**
 * Actions Constants  for Fetching and Recieving News ById 
 */
export const FETCH_NEWS_BY_ID ='FETCH_NEWS_BY_ID';
export const RECEIEVE_NEWS_BY_ID  ='RECEIEVE_NEWS_BY_ID';

/**
 * 
 */
export const DO_LOGIN_USER='DO_LOGIN_USER';
export const LOGIN_USER_SUCCESS  ='LOGIN_USER_SUCCESS';
export const LOGOUT_USER='LOGINOUT_USER';