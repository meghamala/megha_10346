import React, { Component } from 'react';
import { Switch, Route, BrowserRouter } from 'react-router-dom';


import Register from './component/register';
import Home from './component/home';
import Clock from './component/clock';
import CarExample from './component/carExample';
import CarDetails from './component/carDetails';
import FetchUsers from './component/FetchUsers';
import FetchPost from './component/FetchPost';
import RandomUsers from './component/RandomUsers';
import DisplayAllEmployee from './component/DisplayAllEmployee';
import DisplayAllProducts from './component/DisplayAllProducts';
import Counter from './component/counter';
import news from './component/news';
import news_details from './component/news_details';
import Login from './component/login';
import AdminDashboard from './component/dashboard ';

const Routes = () => (
    <BrowserRouter>
        <Switch>
            <Route path={'/'} component={Home} exact />
            <Route path={'/clock'} component={Clock} />
            <Route path ={'/carExample'} component={CarExample} exact/>
            <Route path ={'/carExample/:id'} component={CarDetails}exact/>
            <Route path={'/register'} component={Register} />
            <Route path = {'/FetchUsers'} component={FetchUsers}/>
            <Route path ={'/FetchPost'} component ={FetchPost}/>
            <Route path ={'/RandomUsers'} component ={RandomUsers}/>
            <Route path = {'./DisplayAllEmployee'} component ={DisplayAllEmployee}/>
            <Route path ={'./DisplayAllProduct'} component ={DisplayAllProducts}/>
            <Route path ={'/counter'}  component ={Counter}/>
            <Route path ={'/news'} component ={news} exact/>
            <Route path ={'/news/:id'} component ={news_details}/>
            <Route path ={'/Login'} component={Login} exact/>
            <Route path ={'/admin/dashboard'} component ={AdminDashboard}/>
        </Switch>

    </BrowserRouter>
);
export default Routes;