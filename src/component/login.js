import React, { Component } from 'react';
import'./login.css';
import Redirect from'react-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import  * as authActions from '../store/actions/authActions';
class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email:'',
            password:'',
            isSubmitted:false

        };
        this.handleChange = this.handleChange.bind(this);
        this.doLogin = this.doLogin.bind(this);
    }



    handleChange = e => {
        this.setState({ [e.target.name]: e.target.value });
        e.preventDefault();
    }


    // componentDidMount() {
    //     this.props.newsActions.fetchNewsByID( this.props.match.params.id);
    // }
    // componentWillReceiveProps(newProps){
    //     if(newProps.newsItem){
    //         this.setState({
    //             newsItem: newProps.newsItem,

    //         })
    //     }
    // }

    doLogin = e => {
        console.log("login entered");
        this.props.authActions.doLogin({
            email:this.state.email,
            password:this.state.password
        })
    }
    render() {
        if(this.state.isSubmitted){
            if(localStorage.getItem('jwt-token')&&this.props.isLoggedIn===true){
                return <Redirect to ='/admin/dashboard'/>
            }
        }
        // else{
        //     return(
        //         <div>Your are not logged in </div>
        //     )
        // }
        return (


            <React.Fragment>
                <div class="row">
                     
                    <div class="container" style={{backgroundColor:"lightblue",width:"500px",height:"300px"}}>
                    <div class ="col-lg-12" >
                    </div>
                    <br></br>
                    <div class ="col-lg-12" style={{border:"1pxsolidwhite",height:"200px",width:"500px",marginLeft:"100px"}} >
                        <form  onSubmit={this.doLogin} method="post">
                      
                            <label class ="form_field">
                                Email :      <input type="text" name="email" value={this.state.email} placeholder="enter the username" onChange={this.handleChange} />
                               </label>
                            <br></br>
                            <br></br>
                            <label class ="formfield-2">
                                Password:     <input type="password" name="password" value={this.state.password} placeholder="enter the password" onChange={this.handleChange} />
                            </label>
                            <br></br>
                            <a href ="#href"  id="tag1" target="_blank"> <i>ForgotPassword?</i> </a><br></br>

                                               <input type="submit" value="Submit" />
                        
                        </form>
                        </div>
                    </div>

                </div>


            </React.Fragment>
        );
    }

}
function mapStateToProps(state) {
    return {
        isLoggedIn: state.is_logged_in,
    };
}
function mapDispatchToProps(dispatch) {
    return {
        authActions: bindActionCreators(authActions, dispatch),
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(Login);