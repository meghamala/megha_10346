import React, { Component } from 'react';
class FetchUsers extends Component{
    constructor(props){
        super(props);
        this.state = {
            isLoaded:true,
             items:[]
        }
    }
    componentDidMount(){
        fetch('https://jsonplaceholder.typicode.com/users')
        .then (res=>res.json())
        .then(data=>{
        this.setState({
        isLoaded:true,
        items:data
        })

        });
    }
        render (){
          var {isLoaded,items} =this.state;
          if(!isLoaded){
              return <div>Loading......</div>;
        
          }
          return(
<div className ="app">
<ul>
    {items.map( item=>(
        <li key ="{items.id}>">
        Name :{item.name} | Email : {item.email}
        </li>
    ))}
    
    </ul>      
    
</div>
          )
        }
      

}

export default FetchUsers;
