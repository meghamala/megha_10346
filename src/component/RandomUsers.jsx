import React, { Component } from 'react';
class RandomUsers extends Component{
    constructor(props){
        super(props);
        this.state = {
            isLoaded:true,
             items:[]
        }

    }
    componentDidMount(){
        console.log("there are randomUsers");
        fetch('https://randomuser.me/api/?result')
        .then( res => {
            return res.json();
        })
        .then( data => {
        let users = data.results.map(pic => {
        return (
        <div key={pic.results}>
        <img src={pic.picture.large}></img>
        </div>
        )
        })
        this.setState({
        userdetails:users
    })
    })
}
render (){
    return(
        <div className ="app">
        {this.state.userdetails}
        </div>
    );
    }
}
export default RandomUsers;