import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as newsActions from '../store/actions/newsActions';
import counter from './counter';
import './news.css';
class News extends Component {

    constructor(props) {
        super(props);
    }
    componentDidMount() {
        this.props.newsActions.fetchNews();
    }
    render() {
        return (
            <React.Fragment>
                {

                    (this.props.newsItems)
                        ? <div>
                            <div class="row">
                                        <div class="container-fluid">
                                            <div class="col-sm-lg-12" id="home_nav">
                                                <ul class="home_header">
                                                    <li><h1 style={{ fontFamily: "monospace", color: "white", fontSize: "30", marginTop: "-10px" }}><a>AidForCancer</a></h1></li>
                                                    <li style={{ marginLeft: "500px" }}><a>Information</a></li>
                                                    {/* <li class="header_item" style={{ marginLeft: "500px" }}><a href={`/news/${item._id}`}>information</a></li> */}
                                                    <li class="header_item1"><a href="#news">Options</a></li>
                                                    <li class="header_item2"><a href="#contact">Forum</a></li>
                                                    <li class="header_item3"><a href="#about">Volunteer</a></li>
                                                    <li class="header_item4"><a href="#about">SeekHelp</a></li>
                                                    <li class="header_item4"><button class="b1" style={{ backgroundColor: "#4b7a64", marginTop: "15px" }}> SignIn </button> </li>

                                                </ul>
                                            </div>
                                        </div>
                                    </div>




                            {this.props.newsItems.map(item => (



                                <div key={item._id}>
                                    {/* <div class="row">
                                        <div class="container-fluid">
                                            <div class="col-sm-lg-12" id="home_nav">
                                                <ul class="home_header">
                                                    <li><h1 style={{ fontFamily: "monospace", color: "white", fontSize: "30", marginTop: "-10px" }}><a>AidForCancer</a></h1></li>
                                                    <li class="header_item" style={{ marginLeft: "500px" }}><a href={`/news/${item._id}`}>Iinformation</a></li>
                                                    <li class="header_item1"><a href="#news">Options</a></li>
                                                    <li class="header_item2"><a href="#contact">Forum</a></li>
                                                    <li class="header_item3"><a href="#about">Volunteer</a></li>
                                                    <li class="header_item4"><a href="#about">SeekHelp</a></li>
                                                    <li class="header_item4"><button class="b1" style={{ backgroundColor: "#4b7a64", marginTop: "15px" }}> SignIn </button> </li>

                                                </ul>
                                            </div>
                                        </div>
                                    </div> */}

                                    {/* 
      <a href={`/news/${item._id}`}>{item.heading}</a> */}
                                </div>
                            ))}
                        </div>
                        : <div>

                            loading.....
            </div>
                }
            </React.Fragment>
        );
    }

}
function mapStateToProps(state) {
    return {
        newsItems: state.news,
    };
}
function mapDispatchToProps(dispatch) {
    return {
        newsActions: bindActionCreators(newsActions, dispatch),
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(News);