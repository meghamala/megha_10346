import React, { Component } from 'react';
import {Link} from 'react-router-dom';
class CarExample extends Component{
    constructor(props){
        super(props);
        this.state={
            cars:[]
        }
    }

componentDidMount(){

        const data=[{
            id:101,
            firstname:'megha',
            Lastname :'chillara',
            Emailid:'megha.chillara13@',
            phoneno :'8121779355',
            date_of_birth:'12/10/1996',
            buying_details:'sold'
        },
    {
        id:102,
        firstname:'anusha',
        Lastname :'netha',
        Emailid:'anushanetha@',
        phoneno :'987456211',
        date_of_birth:'11/10/1996',
        buying_details:'sold'
    },
    {
        id:103,
        firstname:'janhavi',
        Lastname :'sagi',
        Emailid:'janhavisagi@',
        phoneno :'987456211',
        date_of_birth:'15/12/1996',
        buying_details:'sold'
    },
    {
        id:104,
        firstname:'arun',
        Lastname :'kumari',
        Emailid:'arunkumari@',
        phoneno :'987456211',
        date_of_birth:'12/12/1996',
        buying_details:'sold'
    },
    {
        id:105,
        firstname:'harshita',
        Lastname :'rao',
        Emailid:'harshitapolsani@',
        phoneno :'9177995290',
        date_of_birth:'12/12/1996',
        buying_details:'sold'
    }  

];
this.setState({ cars:data});

    }
render(){
    console.log("car example render");
    const carNode= this.state.cars.map((car)=>{
    return(
     <Link to={`/carExample/`+car.id}
     className="list-group-item"
     key={car.id}>
     {car.firstname}
     </Link>
    )
});
return(
    <div>

        <h2>CarSoldDetails </h2>

        <div className="list-group">
        {carNode}
        </div>
        </div>

);
}
}
export default CarExample;