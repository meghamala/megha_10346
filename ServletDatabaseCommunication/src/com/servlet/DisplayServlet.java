package com.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bean.Employee;
import com.service.EmployeeService;
import com.service.EmployeeServiceImp1;

/**
 * Servlet implementation class DisplayServlet
 */
@WebServlet("/DisplayServlet")
public class DisplayServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DisplayServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		
		
		RequestDispatcher rd = request.getRequestDispatcher("display.jsp");
		 EmployeeService empservice= new EmployeeServiceImp1();
		List<Employee> emplist = empservice.fetchAllStudents();
		request.setAttribute("emplist", emplist);
		if(emplist.size()!=0)
		{
		rd.forward(request, response);
		
	}
		else {
			response.sendRedirect("errordisplay.html");
		}
}
	}

