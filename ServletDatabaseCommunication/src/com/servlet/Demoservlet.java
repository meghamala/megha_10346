package com.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bean.Employee;
import com.service.EmployeeService;
import com.service.EmployeeServiceImp1;

/**
 * Servlet implementation class Demoservlet
 */
@WebServlet("/Demoservlet")
public class Demoservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Demoservlet() {
        super();
    }
    /*** @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int Employee_id= Integer.parseInt(request.getParameter("id"));
		Employee employee = new Employee();
          EmployeeService service = new EmployeeServiceImp1();
		   employee = service.findById(Employee_id);
		   System.out.println(employee);
		RequestDispatcher rd = request.getRequestDispatcher("search_result.jsp");
		  if(employee!= null)
		{
			request.setAttribute("employee", employee);
			rd.forward(request, response);
		}
		else {
			response.sendRedirect("errorsearch.html");
		}
		
	}

}
