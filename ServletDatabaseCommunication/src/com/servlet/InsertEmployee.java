package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bean.Account;
import com.bean.Employee;
import com.service.EmployeeService;
import com.service.EmployeeServiceImp1;

/**
 * Servlet implementation class InsertEmployee
 */
@WebServlet("/InsertEmployee")
public class InsertEmployee extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertEmployee() {
        super();
 
    }
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// requesting the  methods to get the values
		int emp_id= Integer.parseInt(request.getParameter("id"));
		String emp_name = request.getParameter("name");
		String emp_addr = request.getParameter("address");
		int employee_Age = Integer.parseInt(request.getParameter("age"));
		Long employee_salary = Long.parseLong(request.getParameter("salary"));
		String employee_branch = request.getParameter("branch");
		String employee_designation= request.getParameter("designation");
		
		
	    Employee employee =new Employee();
	    employee.setId(emp_id);
		employee.setName(emp_name);
		employee.setName(emp_addr);
        employee.setAge(employee_Age);
        Account account = new Account();
        account.setSalary(employee_salary);
        account.setBranch(employee_branch);
        account.setDesignation(employee_designation);
        
        employee.setAccount(account);
      	
	    // implementing service method by calling the class
	    @SuppressWarnings("unused")
		RequestDispatcher rd = request.getRequestDispatcher("Insert.jsp");
	    EmployeeService empservice= new EmployeeServiceImp1();
		boolean result = empservice.insertEmployee(employee);
		
	        if(result ){  
	        	
	    	    rd.forward(request, response);
	    	    
	     
	        }else{  
	            response.sendRedirect("error.html"); 
	        }  
	
		
		
	}
	
}
