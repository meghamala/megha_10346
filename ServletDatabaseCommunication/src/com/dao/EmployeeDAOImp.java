package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.bean.Account;
import com.bean.Employee;
import com.util.DBUtil;

/**
 * 
 * @author MEGHA this class is used to implement the insert method into database
 */
public class EmployeeDAOImp implements EmployeeDAO {
	/**
	 * this method is used to
	 */

	public boolean createEmployee(Employee employee) {

		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		// query to the database
		String sql = "insert into employee values(?,?,?,?,?,?,?)";
		try {
			// setting the elements
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, employee.getId());
			ps.setString(2, employee.getName());
			ps.setString(3, employee.getAddress());
			ps.setInt(4, employee.getAge());
//			Account account = new Account();
//			ps.setLong(5, employee.z);
//			ps.setString(6, account.getBranch());
//			ps.setString(7, account.getDesignation());
			ps.setLong(5, employee.getAccount().getSalary());
			ps.setString(6, employee.getAccount().getBranch());
		    ps.setString(7, employee.getAccount().getDesignation());
			// to rows effected
			int rowsEffected = ps.executeUpdate();
			if (rowsEffected >= 1) {
			
				result = true;
			}

		} catch (SQLException e) {

		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return result;
	}

	/**
	 * this method to get the employee id
	 */
	public Employee searchById(int id) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Employee employee = null;
		String sql = "select * from employee  where emp_id = ?";
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			while (rs.next()) {
				Account account =new Account();
				account.setSalary(rs.getLong(5));
                account.setBranch(rs.getString(6));
				account.setDesignation(rs.getString(7));
				employee = new Employee();
				employee.setId(rs.getInt(1));
				employee.setName(rs.getString(2));
				employee.setAddress(rs.getString(3));
				employee.setAge(rs.getInt(4));
				employee.setAccount(account);
				System.out.println("elements displayed");

			}
		} catch (SQLException e) {

		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return employee;
	}

	/**
	 * this method to get the employee list
	 */
	public List<Employee> getAllEmployees() {

		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		Employee emp = null;
		List<Employee> emplist = new ArrayList<Employee>();
		String sql = "select * from employee";
		try {
			con = DBUtil.getCon();
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				emp = new Employee();
				emp.setId(rs.getInt(1));
				emp.setName(rs.getString(2));
				emp.setAddress(rs.getString(3));
				emp.setAge(rs.getInt(4));
				Account account = new Account();
				account.setBranch(rs.getString(6));
				account.setSalary(rs.getLong(5));
				account.setDesignation(rs.getString(7));
				emp.setAccount(account);
				emplist.add(emp);
			}
		} catch (SQLException e) {

		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return emplist;
	}

}
