package com.dao;

import java.util.List;

import com.bean.Employee;

/**
 * 
 * @author MEGHA this interface is used to define the methods
 */
public interface EmployeeDAO {
	// define method call employee to delete
	boolean createEmployee(Employee employee);

	// define method call employee to search
	Employee searchById(int id);

	// define method call employee to get the list
	List<Employee> getAllEmployees();

}
