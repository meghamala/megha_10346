package com.service;

import java.util.List;
import com.bean.Employee;
import com.dao.EmployeeDAO;
import com.dao.EmployeeDAOImp;
/**
 * 
 * @author MEGHA
 * this class provides service to implement the insert method  
 */
public class EmployeeServiceImp1 implements EmployeeService {
    /**
     * method to implement the insert operation
     */
	public boolean insertEmployee(Employee employee) {
		
		EmployeeDAO employeeDAO = new EmployeeDAOImp();
		
		
		boolean result = employeeDAO.createEmployee(employee);
		
		return result;
	}

	@Override
	/**
	 * method to implement to find the id
	 */
	public Employee findById(int id) {
		EmployeeDAO employeeDAO = new EmployeeDAOImp();
	
		Employee employee = employeeDAO.searchById(id);
		return employee;
	}

	@Override
	/**
	 * method to fetch the data
	 */
	public List<Employee> fetchAllStudents() {
		EmployeeDAO employeeDAO = new EmployeeDAOImp();
		List <Employee> emplist = employeeDAO.getAllEmployees();
		
		return emplist;
	}

	
	
	
}
