package com.service;

import java.util.List;

import com.bean.Employee;
/** 
 * 
 * @author MEGHA
 * this service interface to define the service
 */
public interface EmployeeService {
 
   boolean insertEmployee( Employee employee );
	
	 Employee findById(int id);
	
	List<Employee> fetchAllStudents();
	
	
	
}
