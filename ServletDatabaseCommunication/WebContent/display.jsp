<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h2>Data</h2>
<c:forEach var ="employee" items="${requestScope.emplist}">
<b>Id: ${ employee.id }</b>
<b>Name: ${ employee.name }</b>
<b>Address: ${ employee.address}</b>
<b>Age: ${ employee.age }</b>
<b>Salary: ${ employee.account.salary }</b>
<b>Branch: ${ employee.account.branch }</b>
<b>Designation: ${ employee.account.designation }</b>
<hr>
</c:forEach>
</body>
</html>