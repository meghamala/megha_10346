import * as allActions from '../actions/actionConstant';

 const initialState  = {
     news :[],
     
 }
 export default function newsReducer(state = initialState,action){
     switch(action.type){

         case allActions.FETCH_NEWS:
          console.log("datafetched");
         return action;

         case allActions.RECEIVE_NEWS:
           console.log("datareceived");
         return{

             ...state,
             news : action.payload.articles,

         };
         
         default : return state ;   
     }
 }