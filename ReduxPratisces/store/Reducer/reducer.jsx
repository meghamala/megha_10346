const {createStore} = require('redux');

const initialState ={
    age:21
};

const reducer =( state=initialState,action)=>
{
    const newState ={...state};
  if(action.type=='ADD'){
      newState.age +=1;
  }
  else(action.type ==='SUBTRACT')
  {
      newState.age -=1;

  }
  return newState;
}
const store = createStore(reducer);
store.subscribe(()=>{
console.log("StateChanged"+JSON.stringify(store.getState()))
})
store.dispatch({type:'ADD'});
store.dispatch({type:'SUBTRACT'});


